function ejercicio07(dia) {
    var arreglo=["Lunes","Martes","Miercoles","Jueves","Viernes","Sábado","Domingo"];
    var objeto={};
    objeto.dia_actual=dia;
    if(dia==='Lunes'){
        objeto.dia_anterior=arreglo[6];
        objeto.dia_siquiente=arreglo[2];
    }else if(dia==='Domingo'){
        objeto.dia_anterior=arreglo[5];
        objeto.dia_siquiente=arreglo[1];
    }else{
        for(var i=1;i<arreglo.length-1;i++){
            if(dia===arreglo[i]){
                objeto.dia_anterior=arreglo[i-1];
                objeto.dia_siquiente=arreglo[i+1];
            }
        }
    }
    return objeto;
    
}

ejercicio07('Viernes');

